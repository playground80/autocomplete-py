import redis
import util.constants

class RedisConnection:
  def __init__(self):
    self.redisclient = redis.Redis(host = util.constants.redisHost, port = util.constants.redisPort, db = 0)

  def getInstance(self):
    return self.redisclient
