# autocomplete-py

Autocomplete system with python backend and react frontend 

Backend - Python Flask Server with autocomplete implemented with Trie DS. Redis is used to cache previous query patterns along with the responses. APIs to add / delete words to the list. Cache key invalidation on add / delete of words 

Frontend - React JS - UI to query for words and 5 autocomplete suggestions displayed 


Steps to start the service

**Step 1** Install all python dependencies

_pip install -r requirements.txt_

**Step 2** Start redis with the following command

_brew services start redis_

**Step 3** Please run the following command in the root # folder of autocomplete-py to start server in local. This would start the server in localhost:5000

_python server.py_

**Step 4** Goto search-bar folder and do an npm install 

_npm i_

**Step 5** Within search-bar start react app with the below command. This would open the web app in localhost:3000

_npm start_

---------------------------------------------------------------------------------------------------------------------------------------------------

**Sample Curls**

Search for a pattern (Hit the following in your browser) - http://localhost:5000/query?pattern=a

Curl to Insert a word - 

curl --location --request POST 'http://127.0.0.1:5000/insert' \
--header 'Content-Type: application/json' \
--header 'cache-control: no-cache,no-cache' \
--data-raw '{
    "word": "helloworld"
}'

Curl to delete a word - 

curl --location --request DELETE 'http://127.0.0.1:5000/delete' \
--header 'Content-Type: application/json' \
--header 'cache-control: no-cache,no-cache' \
--data-raw '{
    "word": "helloworld"
}'






