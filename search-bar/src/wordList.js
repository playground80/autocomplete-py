import './App.css';
import React from 'react';

class WordList extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    var words = this.props.wordList
    words = words.slice(0, 5)      
    return (
      <div className = "list-container">
      <p className = "bold">Top Auto Suggested Words</p>
      {
        words.map((data,index) => {
            return (
              <div className = "item-container" key={data}>
                <li>{data}</li>
              </div>  
           )  
        }) 
      }
    </div>
    )
  } 
}

export default WordList