import './App.css';
import React from 'react';

class SearchBar extends React.Component {
  constructor(props) {
    super(props)
  }

  onChangeEvent = async(data) => {
    await this.props.onChangeEvent(data.target.value)
  }

  render() {
    return(<input className = "gcse-searchbox-only"
     key="searchkey"
     placeholder={"SEARCH"}
     onChange= {this.onChangeEvent}
    />)
  }
}

export default SearchBar