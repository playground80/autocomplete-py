import './App.css';
import React, { useState, useEffect } from 'react';
import SearchBar from './searchBar';
import WordList from './wordList';

class SearchParent extends React.Component {
  constructor(props) {
    super(props)
    this.state = {wordList: []}
  }

  handleCallBack = async(input) => {
    if(input == "") {
      this.setState({wordList: []})
      return
    }
    await fetch('http://localhost:5000/query?pattern='+input)
    .then(response => response.json())
    .then(data => {
      this.setState({wordList: data.results})
    })
  }

  render() {
    return(
      <div className = "container">
        <SearchBar onChangeEvent = {this.handleCallBack}/>
        <WordList wordList  = {this.state.wordList}/>
      </div>
    )
  }
}

export default SearchParent