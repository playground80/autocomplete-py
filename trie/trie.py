import util.constants
from redisutil.redisinit import RedisConnection

class TrieNode:
  def __init__(self, value):
    self.data = value
    self.children = [None for _ in range(26)]
    self.eow = False

class Trie:
  def __init__(self):
    self.root = TrieNode(" ")
    self.redisConnection = RedisConnection().getInstance()

  def _isEmpty(self, node):
    lent = 0
    for i in node.children:
      if(not i):
        lent += 1
    return lent == 26

  def _getIndex(self, char):
    return ord(char) - ord('a')

  def insertWord(self, word):
    self._insertWordUtil(word)

  def _insertWordUtil(self, word):
    word = word.lower()
    node = self.root
    for i in word:
      idx = self._getIndex(i)
      if(not node.children[idx]):
        node.children[idx] = TrieNode(i)
      node = node.children[idx]
    node.eow = True

  def _queryUtil(self, pattern, node, res):
    if(node.eow):
      res.append(pattern)
    for i in range(len(node.children)):
      if(node.children[i] is not None):
        self._queryUtil(pattern + node.children[i].data, node.children[i], res)

  def query(self, pattern):
    res = []
    node = self.root
    pattern = pattern.lower()
    if(self.redisConnection.scard(pattern)):
      redisResponse = self.redisConnection.smembers(pattern)
      for i in redisResponse:
        res.append(i.decode("utf-8"))
      return res
    for i in pattern:
      idx = self._getIndex(i)
      if(not node.children[idx]):
        return res
      node = node.children[idx]
    self._queryUtil(pattern, node, res)
    for i in res:
      self.redisConnection.sadd(pattern, i)
    return res

  def populateTrie(self):
    for i in util.constants.words:
      self._insertWordUtil(i)

  def _deleteWordUtil(self, word, node, i, track):
    if(i == len(word)):
      if(self._isEmpty(node)):
        node = None
      else:
        track[0] = False
        node.eow = False
      return node
    idx = self._getIndex(word[i])
    if(node.children[idx]):
      node.children[idx] = self._deleteWordUtil(word, node.children[idx], i+1, track)
    if(track[0] and self._isEmpty(node) and not node.eow):
      node = None
    else:
      track[0] = False
    return node

  def deleteWord(self, word):
    word = word.lower()
    track = [True]
    self.root = self._deleteWordUtil(word, self.root, 0, track)
    for k in range(0, len(word)+1):
      i = word[0:k]
      if(self.redisConnection.scard(i.lower())):
        self.redisConnection.srem(i.lower(), word)


