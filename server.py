from flask import Flask, jsonify, request
from flask_cors import CORS
from trie.trie import Trie

app = Flask(__name__)
CORS(app)

@app.route('/insert', methods=['POST'])
def insertWord():
  trie.insertWord(request.get_json()['word'])
  return jsonify({"status": "Success", "message": "Data Inserted To Trie"})

@app.route('/query', methods=['GET'])
def query():
  pattern = request.args.get('pattern')
  return jsonify(results=trie.query(pattern))

@app.route('/delete', methods=['DELETE'])
def deleteWord():
  trie.deleteWord(request.get_json()['word'])
  return jsonify({"status": "Success", "message": "Data Deleted From Trie"})

if __name__ == '__main__':
  trie = Trie()
  trie.populateTrie()
  app.run(port = 5000, debug=True)
